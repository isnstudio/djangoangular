from django.db import models

# Create your models here.

class Category(models.Model):

    name = models.CharField(max_length=50)

    class Meta:
        verbose_name = "Category"
        verbose_name_plural = "Categorys"

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name


class Comment(models.Model):

    TRIAL = 'trial'
    SIGNEDUP = 'signedup'
    EXPIRED = 'expired'

    TXT_CHOICES = [
        (TRIAL, 'Trial'),
        (SIGNEDUP, 'Signed up'),
        (EXPIRED, 'Expired')
    ]

    PUBLICADO = 1
    REVISION = 2
    CANCELADO = 3

    INT_CHOICES = [
        (PUBLICADO, 'Publicado'),
        (REVISION, 'Revision'),
        (CANCELADO, 'Cancelado')
    ]

    title = models.CharField(max_length=50)
    content = models.CharField(max_length=50)
    category = models.ForeignKey(Category)
    txt_choice = models.CharField(choices=TXT_CHOICES, max_length=50)
    int_choice = models.SmallIntegerField(choices=INT_CHOICES)

    class Meta:
        verbose_name = "Comment"
        verbose_name_plural = "Comments"

    def __str__(self):
        pass