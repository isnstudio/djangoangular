from django import forms
from django.forms import ModelForm, Textarea, TextInput, Select

from models import Comment

class CommentForm(forms.ModelForm):

    # title = forms.CharField(widget=forms.TextInput(attrs={'ng-model':'record.title'}))
    # content = forms.CharField(widget=forms.TextInput(attrs={'ng-model':'record.content'}))
    # category = forms.ModelChoiceField(widget=forms.TextInput(attrs={'ng-model':'record.category'}))
    
    class Meta:
        model = Comment
        fields = (
            'title', 
            'content',
            'category',
            'txt_choice',
            'int_choice',
            )
        widgets = {
            'title': TextInput(attrs={'ng-model': 'record.title'}),
            'content': TextInput(attrs={'ng-model': 'record.content'}),
            'category': Select(attrs={'ng-model': 'record.category', 'ng-options':'category.pk as category.name for category in categories'}),
            'txt_choice': Select(attrs={'ng-model': 'record.txt_choice', 'ng-options':'txt_choice[0] as txt_choice[1] for txt_choice in txt_choices'}),
            'int_choice': Select(attrs={'ng-model': 'record.int_choice', 'ng-options':'int_choice[0] as int_choice[1] for int_choice in int_choices'}),
        }