from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response,get_object_or_404
from django.template.context import RequestContext

from models import Comment, Category
from rest_framework import viewsets, filters
from rest_framework.decorators import detail_route, list_route
from rest_framework.response import Response

from serializers import CategorySerializer, CommentSerializer

from forms import CommentForm

class CategoryViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class CommentViewSet(viewsets.ModelViewSet):

    @list_route(methods=['get'])
    def txt_choices(self, request, pk=None):
        return Response({'results': Comment.TXT_CHOICES })

    @list_route(methods=['get'])
    def int_choices(self, request, pk=None):
        return Response({'results': Comment.INT_CHOICES })

    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
    filter_backends = (filters.OrderingFilter,)
    ordering_fields = ('pk','title', 'category',)
    ordering = ('-pk',)    


def comments(request):
    form = CommentForm()
    template = 'comments.html'
    return render_to_response(template, context_instance=RequestContext(request,locals()))