# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0002_auto_20150208_2216'),
    ]

    operations = [
        migrations.RenameField(
            model_name='comment',
            old_name='int_choices',
            new_name='int_choice',
        ),
        migrations.RenameField(
            model_name='comment',
            old_name='txt_choices',
            new_name='txt_choice',
        ),
    ]
