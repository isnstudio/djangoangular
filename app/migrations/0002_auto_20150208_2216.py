# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='comment',
            name='int_choices',
            field=models.SmallIntegerField(default=1, choices=[(1, b'Publicado'), (2, b'Revision'), (3, b'Cancelado')]),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='comment',
            name='txt_choices',
            field=models.CharField(default='trial', max_length=50, choices=[(b'trial', b'Trial'), (b'signedup', b'Signed up'), (b'expired', b'Expired')]),
            preserve_default=False,
        ),
    ]
