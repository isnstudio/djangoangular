from models import Comment, Category
from rest_framework import serializers
from django.db import models

class CategorySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Category
        fields = ('pk', 'url', 'name',)


class CommentSerializer(serializers.HyperlinkedModelSerializer):

    category = serializers.PrimaryKeyRelatedField(read_only=False, queryset=Category.objects.all())

    class Meta:
        model = Comment
        fields = ('pk', 'url', 'title', 'content', 'category', 'txt_choice', 'int_choice',)
        depth = 1