from django.conf.urls import patterns, include, url
from django.contrib import admin

from rest_framework import routers

from app import views as app_views

router = routers.DefaultRouter()
router.register(r'categories', app_views.CategoryViewSet)
router.register(r'comments', app_views.CommentViewSet)
# router.register(r'comments/form_data', app_views.CommentViewSet.form_data())

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'rest.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^', include(router.urls)),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),

    url(r'^app/comments/$', 'app.views.comments', name='home_comments'),
)
